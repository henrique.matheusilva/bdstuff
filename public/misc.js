function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(document.location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

$(document).ready(() => {
    setTimeout(() => {
        const plugin = getUrlParameter('plugin');
        const pluginEl = document.getElementById(plugin);
        if (!pluginEl) return;
        const position = $(pluginEl).offset().top;
        $('main').animate(
            {
                scrollTop: position
            },
            500
        );
    }, 100);
});
